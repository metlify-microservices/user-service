package de.htw.metlify.userservice.models.requests.updates;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateSexRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    @NotEmpty
    @NotBlank
    private String newSex;

    public UpdateSexRequest(int id, String newSex) {
        this.id = id;
        this.newSex = newSex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewSex() {
        return newSex;
    }

    public void setNewSex(String newSex) {
        this.newSex = newSex;
    }
}
