package de.htw.metlify.userservice.annotations;


import de.htw.metlify.userservice.utils.UniqueUsernameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = UniqueUsernameValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface UniqueUsername {

    String message() default "This Username is already in use";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
