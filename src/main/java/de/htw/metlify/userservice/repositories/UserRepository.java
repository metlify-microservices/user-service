package de.htw.metlify.userservice.repositories;

import de.htw.metlify.userservice.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {
    User findById(int id);
    User findByIdAndIsPrivate(int id, boolean isPrivate);
    User findByUsername(String username);
    User findByUsernameAndIsPrivate(String username, boolean isPrivate);
    User findByEmail(String email);
    User findByEmailAndIsPrivate(String email, boolean isPrivate);
    List<User> findAllByIsPrivate(boolean isPrivate);
    List<User> findByLocationAndIsPrivate(String location, boolean isPrivate);
    List<User> findByFavouriteGenreAndIsPrivate(String favouriteGenre, boolean isPrivate);
    List<User> findByFavouriteBandAndIsPrivate(String favouriteBand, boolean isPrivate);

    @Query(value="SELECT song_id FROM user_song WHERE user_id = :#{#userId}", nativeQuery = true)
    Set<Integer> findLikedSongsFromUser(@Param("userId") Integer userId);
    @Query(value="SELECT playlist_id FROM user_playlist WHERE user_id = :#{#userId}", nativeQuery = true)
    Set<Integer> findLikedPlaylistsFromUser(@Param("userId") Integer userId);
    @Transactional
    @Modifying
    @Query(value="DELETE FROM user_song WHERE user_id = :#{#userId}", nativeQuery = true)
    void deleteLikedSongs(@Param("userId") Integer id);
    @Transactional
    @Modifying
    @Query(value="DELETE FROM user_playlist WHERE user_id = :#{#userId}", nativeQuery = true)
    void deleteLikedPlaylists(@Param("userId") Integer id);

    @Transactional
    @Modifying
    @Query(value = "Insert into user_song values (:#{#userId},:#{#songId})", nativeQuery = true)
    void addLikedSongs(@Param("userId") int userId,@Param("songId") Integer newLikedSongId);
    @Transactional
    @Modifying
    @Query(value = "Insert into user_playlist values (:#{#userId},:#{#playlistId})", nativeQuery = true)
    void addLikedPlaylist(@Param("userId") int userId,@Param("playlistId") Integer newLikedPlaylistId);
    @Transactional
    @Modifying
    @Query(value="DELETE FROM user_playlist WHERE playlist_id = :#{#playlistId}", nativeQuery = true)
    void deleteLikedPlaylistsByPlaylistId(@Param("playlistId") int playlistId);

    @Transactional
    @Modifying
    @Query(value="DELETE FROM user_song WHERE song_id = :#{#songId}", nativeQuery = true)
    void deleteLikedSongsBySongId(@Param("songId") int songId);
}
