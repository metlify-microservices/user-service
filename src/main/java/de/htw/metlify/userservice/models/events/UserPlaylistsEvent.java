package de.htw.metlify.userservice.models.events;

import java.util.Set;

public class UserPlaylistsEvent {
    private Set<Integer> playlistIds;

    public UserPlaylistsEvent(Set<Integer> playlistIds) {
        this.playlistIds = playlistIds;
    }

    public UserPlaylistsEvent() {
    }

    public Set<Integer> getPlaylistIds() {
        return playlistIds;
    }

    public void setPlaylistIds(Set<Integer> playlistIds) {
        this.playlistIds = playlistIds;
    }
}
