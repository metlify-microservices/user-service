package de.htw.metlify.userservice.services;


import de.htw.metlify.userservice.models.User;
import de.htw.metlify.userservice.models.requests.updates.NewPasswordRequest;
import de.htw.metlify.userservice.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public RegistrationServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public User registerUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        return userRepository.save(user);
    }

    @Override
    public User updateUserPassword(NewPasswordRequest request) {
        String newPassword = bCryptPasswordEncoder.encode(request.getNewPassword());
        User userFromDB = userRepository.findByIdAndIsPrivate(request.getId(), false);
        userFromDB.setPassword(newPassword);

        return userRepository.save(userFromDB);
    }
}
