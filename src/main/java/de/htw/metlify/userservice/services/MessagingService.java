package de.htw.metlify.userservice.services;

import java.util.Set;

public interface MessagingService {
    boolean areSongsInDatabase(Set<Integer> songIds);
    boolean arePlaylistsInDatabase(Set<Integer> playListIds);
    void deleteUserInAllServices(Integer id);

    Set<Integer> getSongsFromUser(Integer userId);

    Set<Integer> getPlaylistsFromUser(Integer userId);
}
