package de.htw.metlify.userservice.utils;

import de.htw.metlify.userservice.annotations.UniqueUsername;
import de.htw.metlify.userservice.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {
    @Autowired
    private UserService userService;

    @Override
    // this if else statement is needed because otherwise the code crashes with an NullPointerException
    // the NullPointerException is triggered because userService gets randomly null for whatever reason
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (userService != null) {
            return value != null && !userService.isUsernameAlreadyInUse(value);
        }
        else {
            return true;
        }
    }
}
