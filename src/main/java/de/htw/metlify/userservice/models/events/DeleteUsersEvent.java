package de.htw.metlify.userservice.models.events;

public class DeleteUsersEvent {
    private int userId;

    public DeleteUsersEvent(int userId) {
        this.userId = userId;
    }

    public DeleteUsersEvent() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
