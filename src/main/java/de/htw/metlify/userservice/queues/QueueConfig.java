package de.htw.metlify.userservice.queues;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;


@Configuration
public class QueueConfig {

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("confirmation.songs");
    }

    @Bean
    public DirectExchange songsDirectExchange() {
        return new DirectExchange("exchange.song");
    }

    @Bean DirectExchange playlistsDirectExchange() {
        return new DirectExchange("exchange.playlist");
    }

    @Bean
    public Queue getUserIdResponse() {
        return new Queue("getUserIdResponse");
    }

    @Bean
    public Queue deletePlaylist() {
        return new Queue("deletePlaylist");
    }

    @Bean
    public Queue deleteSong() {
        return new Queue("deleteSongUser");
    }

    @Bean
    public Queue getUserIdFromArtistNameResponse() {
        return new Queue("getUserIdFromArtistNameResponse");
    }


    @Bean
    public Binding getUserIdBinding(@Qualifier("songsDirectExchange") DirectExchange directExchange, @Qualifier("getUserIdResponse") Queue getUserIdQueue) {
        return BindingBuilder.bind(getUserIdQueue)
                .to(directExchange)
                .with("userId.header.song");
    }

    @Bean
    public Binding getUserIdPlaylistBinding(@Qualifier("playlistsDirectExchange") DirectExchange directExchange, @Qualifier("getUserIdResponse") Queue getUserIdQueue) {
        return BindingBuilder.bind(getUserIdQueue)
                .to(directExchange)
                .with("userId.header.playlist");
    }

    @Bean
    public Binding deletePlaylistBinding(@Qualifier("playlistsDirectExchange") DirectExchange directExchange, @Qualifier("deletePlaylist") Queue deletePlaylistQueue) {
        return BindingBuilder.bind(deletePlaylistQueue)
                .to(directExchange)
                .with("delete.playlist");
    }

    @Bean
    public Binding deleteSongBinding(@Qualifier("songsDirectExchange") DirectExchange directExchange, @Qualifier("deleteSong") Queue deleteSongQueue) {
        return BindingBuilder.bind(deleteSongQueue)
                .to(directExchange)
                .with("delete.song");
    }

    @Bean
    public Binding getUserIdByArtistNameBinding(@Qualifier("songsDirectExchange") DirectExchange directExchange, @Qualifier("getUserIdFromArtistNameResponse") Queue getUserIdFromArtistQueue) {
        return BindingBuilder.bind(getUserIdFromArtistQueue)
                .to(directExchange)
                .with("userId.artistName");
    }

    @Bean
    public Binding getUserIdByOwnerNameBinding(@Qualifier("playlistsDirectExchange") DirectExchange directExchange, @Qualifier("getUserIdFromArtistNameResponse") Queue getUserIdFromArtistQueue) {
        return BindingBuilder.bind(getUserIdFromArtistQueue)
                .to(directExchange)
                .with("userId.artistName");
    }

    @Bean
    public MessageConverter jackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
