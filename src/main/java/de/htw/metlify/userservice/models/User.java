package de.htw.metlify.userservice.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.htw.metlify.userservice.annotations.UniqueEmail;
import de.htw.metlify.userservice.annotations.UniqueUsername;


import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @Size(min = 2, max = 50, message = "Your username should be atleast 2 Characters and maximum 40")
    @UniqueUsername
    private String username;
    @NotNull
    @Email
    @UniqueEmail
    private String email;
    @NotNull
    @Size(min = 6, message = "The Password must be at least 6 Characters and maximum 40")
    private String password;
    private int age;
    private String sex;
    private String location;
    private String favouriteGenre;
    private String favouriteBand;
    private boolean isPrivate;
//    @OneToMany(mappedBy = "artist", orphanRemoval = true, cascade = CascadeType.ALL)
//    private Set<Integer> songs;
//    @ManyToMany(cascade = CascadeType.MERGE)
//    @JoinTable(
//            name = "user_song",
//            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "song_id", referencedColumnName = "id")}
//    )
//    private Set<Integer> likedSongs;
//    @OneToMany(mappedBy = "owner", orphanRemoval = true, cascade = CascadeType.ALL)
//    private Set<Integer> playlists;
//    @ManyToMany
//    @JoinTable(
//            name = "user_playlist",
//            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "playlist_id", referencedColumnName = "id")}
//    )
//    private Set<Integer> likedPlaylists;

    public User() {
    }

    public User(int id,
                String username,
                String email,
                String password,
                int age,
                String sex,
                String location,
                String favouriteGenre,
                String favouriteBand,
                boolean isPrivate
//                Set<Integer> songs,
//                Set<Integer> likedSongs,
//                Set<Integer> playlists,
//                Set<Integer> likedPlaylists
    ) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.age = age;
        this.sex = sex;
        this.location = location;
        this.favouriteGenre = favouriteGenre;
        this.favouriteBand = favouriteBand;
        this.isPrivate = isPrivate;
//        this.songs = songs;
//        this.likedSongs = likedSongs;
//        this.playlists = playlists;
//        this.likedPlaylists = likedPlaylists;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFavouriteGenre() {
        return favouriteGenre;
    }

    public void setFavouriteGenre(String favouriteGenre) {
        this.favouriteGenre = favouriteGenre;
    }

    public String getFavouriteBand() {
        return favouriteBand;
    }

    public void setFavouriteBand(String favouriteBand) {
        this.favouriteBand = favouriteBand;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

//    public Set<Integer> getSongs() {
//        return songs;
//    }
//
//    public void setSongs(Set<Integer> songs) {
//        this.songs = songs;
//    }
//
//    public Set<Integer> getLikedSongs() {
//        return likedSongs;
//    }
//
//    public void setLikedSongs(Set<Integer> likedSongs) {
//        this.likedSongs = likedSongs;
//    }
//
//    public Set<Integer> getPlaylists() {
//        return playlists;
//    }
//
//    public void setPlaylists(Set<Integer> playlists) {
//        this.playlists = playlists;
//    }
//
//    public Set<Integer> getLikedPlaylists() {
//        return likedPlaylists;
//    }
//
//    public void setLikedPlaylists(Set<Integer> likedPlaylists) {
//        this.likedPlaylists = likedPlaylists;
//    }
}