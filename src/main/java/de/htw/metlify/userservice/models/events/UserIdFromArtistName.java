package de.htw.metlify.userservice.models.events;

public class UserIdFromArtistName {

    private int userId;

    public UserIdFromArtistName(int userId) {
        this.userId = userId;
    }

    public UserIdFromArtistName() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
