package de.htw.metlify.userservice.models.events;

import java.util.Set;

public class ArePlaylistsInDatabaseEvent {
    private Set<Integer> playlistIds;

    public ArePlaylistsInDatabaseEvent(Set<Integer> playlistIds) {
        this.playlistIds = playlistIds;
    }

    public ArePlaylistsInDatabaseEvent() {
    }

    public Set<Integer> getPlaylistIds() {
        return playlistIds;
    }

    public void setPlaylistIds(Set<Integer> playlistIds) {
        this.playlistIds = playlistIds;
    }
}
