package de.htw.metlify.userservice.models.events;

public class GetUserPlaylistsEvent {
    private int userId;

    public GetUserPlaylistsEvent(int userId) {
        this.userId = userId;
    }

    public GetUserPlaylistsEvent() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
