package de.htw.metlify.userservice.models.requests.updates;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateIsPrivateRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    private boolean isPrivate;

    public UpdateIsPrivateRequest(int id, boolean isPrivate) {
        this.id = id;
        this.isPrivate = isPrivate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(String newFavouriteBand) {
        this.isPrivate = isPrivate;
    }
}
