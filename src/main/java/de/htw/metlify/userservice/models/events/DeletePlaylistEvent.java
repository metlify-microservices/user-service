package de.htw.metlify.userservice.models.events;

public class DeletePlaylistEvent {
    private int playlistId;

    public DeletePlaylistEvent(int playlistId) {
        this.playlistId = playlistId;
    }

    public DeletePlaylistEvent() {
    }

    public int getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(int playlistId) {
        this.playlistId = playlistId;
    }
}
