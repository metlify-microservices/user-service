package de.htw.metlify.userservice.models.requests.updates;

import javax.validation.constraints.*;

public class UpdateEmailRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    @Email
    @NotEmpty
    @NotBlank
    private String newEmail;

    public UpdateEmailRequest(int id, String newEmail) {
        this.id = id;
        this.newEmail = newEmail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }
}
