package de.htw.metlify.userservice.models.requests.updates;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class NewPasswordRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    @Size(min = 6, message = "The new password must be at least 6 characters long")
    private String newPassword;

    public NewPasswordRequest(int id, String newPassword) {
        this.id = id;
        this.newPassword = newPassword;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
