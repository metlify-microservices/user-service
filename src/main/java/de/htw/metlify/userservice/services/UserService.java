package de.htw.metlify.userservice.services;



import de.htw.metlify.userservice.exceptions.UserEmailAlreadyInUseException;
import de.htw.metlify.userservice.exceptions.UsernameAlreadyInUseExcection;
import de.htw.metlify.userservice.models.User;
import de.htw.metlify.userservice.models.UserDTO;
import de.htw.metlify.userservice.models.requests.updates.*;

import java.util.List;
import java.util.Set;

public interface UserService {
    UserDTO findUserById(Integer id);
    List<UserDTO> findAllUsers();
    User findUserByUsername(String username);
    UserDTO updateUsername(UpdateUsernameRequest request) throws UsernameAlreadyInUseExcection;

    UserDTO updateEmail(UpdateEmailRequest request) throws UserEmailAlreadyInUseException;
    UserDTO updateSex(UpdateSexRequest request);
    UserDTO updateLocation(UpdateLocationRequest request);
    UserDTO updateFavouriteGenre(UpdateFavouriteGenreRequest request);
    UserDTO updateFavouriteBand(UpdateFavouriteBandRequest request);
    UserDTO updateIsPrivate(UpdateIsPrivateRequest request);
    UserDTO updateLikedSongs(int userId, Set<Integer> newLikedSongs);
    UserDTO updateLikedPlaylists(int userId, Set<Integer> newLikedPlaylists);
    boolean isEmailAlreadyInUse(String value);
    boolean isUsernameAlreadyInUse(String value);
    List<UserDTO> getUserByParameter(String parameter, String value);
    void deleteUser(Integer id);
    void deleteUserPlaylist(int playlistId);

    void deleteUserSong(int songId);
}
