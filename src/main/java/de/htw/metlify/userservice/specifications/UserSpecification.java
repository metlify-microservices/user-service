package de.htw.metlify.userservice.specifications;

import de.htw.metlify.userservice.models.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class UserSpecification implements Specification<User> {

    private String variableColumnName;
    private String valueToSearchFor;

    public UserSpecification(String variableColumnName, String valueToSearchFor) {
        this.variableColumnName = variableColumnName;
        this.valueToSearchFor = valueToSearchFor;
    }

    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and(criteriaBuilder.equal(root.<String>get(this.variableColumnName), this.valueToSearchFor));
    }
}
