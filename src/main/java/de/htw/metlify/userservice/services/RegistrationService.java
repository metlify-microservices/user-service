package de.htw.metlify.userservice.services;


import de.htw.metlify.userservice.models.User;
import de.htw.metlify.userservice.models.requests.updates.NewPasswordRequest;

public interface RegistrationService {
    User registerUser(User user);
    User updateUserPassword(NewPasswordRequest request);
}
