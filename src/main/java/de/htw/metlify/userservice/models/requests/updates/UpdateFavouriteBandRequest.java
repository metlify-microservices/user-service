package de.htw.metlify.userservice.models.requests.updates;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateFavouriteBandRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    @NotEmpty
    @NotBlank
    private String newFavouriteBand;

    public UpdateFavouriteBandRequest(int id, String newFavouriteGenre) {
        this.id = id;
        this.newFavouriteBand = newFavouriteGenre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewFavouriteBand() {
        return newFavouriteBand;
    }

    public void setNewFavouriteBand(String newFavouriteBand) {
        this.newFavouriteBand = newFavouriteBand;
    }
}
