package de.htw.metlify.userservice.services;

import de.htw.metlify.userservice.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface LoginService extends UserDetailsService {
    User getUserByUsername(String username);
}
