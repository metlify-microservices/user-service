package de.htw.metlify.userservice.services;

import com.sun.xml.bind.v2.runtime.output.SAXOutput;
import de.htw.metlify.userservice.models.events.*;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class QueueProducer implements MessagingService {

    private final RabbitTemplate template;
    private final DirectExchange directExchange;
    public static final String ROUTING_KEY_SONGS_IN_DB = "old.songs";
    public static final String ROUTING_KEY_USER_SONGS = "user.songs";
    public static final String ROUTING_KEY_PLAYLISTS_IN_DB = "old.playlists";
    public static final String ROUTING_KEY_USER_PLAYLISTS = "user.playlists";
    public static final String ROUTING_KEY_DELETE_USER = "delete.user";

    public QueueProducer(RabbitTemplate template, DirectExchange directExchange) {
        this.template = template;
        this.directExchange = directExchange;
    }

    @Override
    @Scheduled(fixedDelay = 3000)
    public boolean areSongsInDatabase(Set<Integer> songIds) {
        AreSongsInDatabaseEvent event = new AreSongsInDatabaseEvent(songIds);

        SongsInDatabaseConfirmationEvent response = template.convertSendAndReceiveAsType(
                directExchange.getName(),
                ROUTING_KEY_SONGS_IN_DB,
                event,
                new ParameterizedTypeReference<>() {
                });

        if (response != null) {
            return response.isAreSongsInDatabase();
        } else {
            return false;
        }
    }

    @Override
    @Scheduled(fixedDelay = 3000)
    public boolean arePlaylistsInDatabase(Set<Integer> playListIds) {
        ArePlaylistsInDatabaseEvent event = new ArePlaylistsInDatabaseEvent(playListIds);

        PlaylistsInDatabaseConfirmationEvent response = template.convertSendAndReceiveAsType(
                directExchange.getName(),
                ROUTING_KEY_PLAYLISTS_IN_DB,
                event,
                new ParameterizedTypeReference<>() {
                });

        if (response != null) {
            System.out.println(response.isArePlaylistsInDatabase());
            return response.isArePlaylistsInDatabase();
        } else {
            return false;
        }
    }

    @Override
    public void deleteUserInAllServices(Integer id) {
        DeleteUsersEvent event = new DeleteUsersEvent(id);
        template.convertAndSend(directExchange.getName(), ROUTING_KEY_DELETE_USER,event);
    }

    @Override
    @Scheduled(fixedDelay = 3000)
    public Set<Integer> getSongsFromUser(Integer userId) {
        GetUserSongsEvent event = new GetUserSongsEvent(userId);

        UserSongsEvent response = template.convertSendAndReceiveAsType(
                directExchange.getName(),
                ROUTING_KEY_USER_SONGS,
                event,
                new ParameterizedTypeReference<>() {
                });

        if (response != null) {
            System.out.println(response.getSongIds());
            return response.getSongIds();
        } else {
            return null;
        }
    }

    @Override
    @Scheduled(fixedDelay = 4000)
    public Set<Integer> getPlaylistsFromUser(Integer userId) {
        GetUserPlaylistsEvent event = new GetUserPlaylistsEvent(userId);

        UserPlaylistsEvent response = template.convertSendAndReceiveAsType(
                directExchange.getName(),
                ROUTING_KEY_USER_PLAYLISTS,
                event,
                new ParameterizedTypeReference<>() {
                });

        if (response != null) {
            System.out.println(response.getPlaylistIds());
            return response.getPlaylistIds();
        } else {
            System.out.println("Playlist Service returned null for user playlists");
            return null;
        }
    }
}
