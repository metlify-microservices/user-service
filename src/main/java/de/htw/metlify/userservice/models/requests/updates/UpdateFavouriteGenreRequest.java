package de.htw.metlify.userservice.models.requests.updates;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class UpdateFavouriteGenreRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    @NotEmpty
    @NotBlank
    private String newFavouriteGenre;

    public UpdateFavouriteGenreRequest(int id, String newFavouriteGenre) {
        this.id = id;
        this.newFavouriteGenre = newFavouriteGenre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewFavouriteGenre() {
        return newFavouriteGenre;
    }

    public void setNewFavouriteGenre(String newFavouriteGenre) {
        this.newFavouriteGenre = newFavouriteGenre;
    }
}
