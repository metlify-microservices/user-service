package de.htw.metlify.userservice.controllers;

import de.htw.metlify.userservice.UserServiceApplication;
import de.htw.metlify.userservice.queues.QueueConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class StatusController {

    @GetMapping("/status")
    public String checkHealth() {
        return "Status online";
    }
}
