package de.htw.metlify.userservice.utils;

import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class JwtTokenValidator {

    private final Environment environment;

    @Autowired
    public JwtTokenValidator(Environment environment) {
        this.environment = environment;
    }

    public Integer getIdFromAuthHeader(String authHeader) {
        String token = authHeader.replace("Bearer", "");
        String userIdString = Jwts.parser()
                .setSigningKey(environment.getProperty("token.secret"))
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        if (userIdString == null) {
            return null;
        }
        else {
            return Integer.parseInt(userIdString);
        }
    }
}
