package de.htw.metlify.userservice.models.events;

public class GetUserIdFromAuthHeaderEvent {

    private String authHeader;

    public GetUserIdFromAuthHeaderEvent(String authHeader) {
        this.authHeader = authHeader;
    }

    public GetUserIdFromAuthHeaderEvent() {
    }

    public String getAuthHeader() {
        return authHeader;
    }

    public void setAuthHeader(String authHeader) {
        this.authHeader = authHeader;
    }
}
