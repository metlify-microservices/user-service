package de.htw.metlify.userservice.annotations;


import de.htw.metlify.userservice.utils.UniqueEmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = UniqueEmailValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface UniqueEmail {

    String message() default "This E-Mail is already in use";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
