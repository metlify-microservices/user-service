package de.htw.metlify.userservice.queues;


import de.htw.metlify.userservice.models.events.*;
import de.htw.metlify.userservice.services.UserService;
import de.htw.metlify.userservice.utils.JwtTokenValidator;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class QueueConsumer {

    @Autowired
    private JwtTokenValidator tokenValidator;

    @Autowired
    private UserService userService;

    @RabbitListener(queues = "getUserIdResponse", concurrency = "3")
    public UserIdFromAuthHeader receiveAuthHeader(GetUserIdFromAuthHeaderEvent event) throws AmqpRejectAndDontRequeueException {
        String authHeader = event.getAuthHeader();

        int userId = tokenValidator.getIdFromAuthHeader(authHeader);
        UserIdFromAuthHeader answer = new UserIdFromAuthHeader(userId);

        return answer;
    }

    @RabbitListener(queues = "getUserIdFromArtistNameResponse" , concurrency = "3")
    public UserIdFromArtistName receiveUserName(GetUserIdFromArtistName event) {
        String artistName = event.getArtistName();
        System.out.println("Request for user id for username: " +artistName);
        int userId = userService.findUserByUsername(artistName).getId();
        System.out.println("this is the userid for for the requested user" + userId);
        UserIdFromArtistName answer = new UserIdFromArtistName(userId);

        return answer;
    }

    @RabbitListener(queues = "deletePlaylist" , concurrency = "3")
    public void receiveDeletePlaylist(DeletePlaylistEvent event) {
        int playlistId = event.getPlaylistId();
        System.out.println("deleting playlist with playlist Id " + playlistId);
        userService.deleteUserPlaylist(playlistId);
    }

    @RabbitListener(queues = "deleteSongUser", concurrency = "3")
    public void receiveDeleteSong(DeleteSongEvent event) {
        int songId = event.getSongId();
        System.out.println("deleting song with song id " + songId);
        userService.deleteUserSong(songId);
    }
}
