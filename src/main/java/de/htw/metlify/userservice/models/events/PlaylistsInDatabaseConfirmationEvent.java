package de.htw.metlify.userservice.models.events;

public class PlaylistsInDatabaseConfirmationEvent {

    private boolean arePlaylistsInDatabase;

    public PlaylistsInDatabaseConfirmationEvent(boolean arePlaylistsInDatabase) {
        this.arePlaylistsInDatabase = arePlaylistsInDatabase;
    }

    public PlaylistsInDatabaseConfirmationEvent() {
    }

    public boolean isArePlaylistsInDatabase() {
        return arePlaylistsInDatabase;
    }

    public void setArePlaylistsInDatabase(boolean arePlaylistsInDatabase) {
        this.arePlaylistsInDatabase = arePlaylistsInDatabase;
    }
}
