INSERT INTO metlify.user (id, username, email, password, age, sex, location, favourite_genre, favourite_band, is_private) VALUES (1, 'peter123', 'peter@gmx.de', '$2a$10$cdfkN4OSYsnv0oF.WDYV0emu5DesaKRIsFkAaOOXce6kHFSZ5An1W', 45, 'male', 'Germany/Berlin', 'Old School', 'Judas Priest', false);
INSERT INTO metlify.user (id, username, email, password, age, sex, location, favourite_genre, favourite_band, is_private) VALUES (2, 'Epic Shadows', 'e.shadows@googlemail.com', '$2a$10$hZsSb.8pAsYoxRIxiV9swOfH5f4J4BDqblot6Byq6wrG1axLSxMcG', 23, 'male', 'England/London', 'Progressive Metal', 'Haken', false);

--
-- INSERT INTO metlify.song (id, title, user_id, released, label, genre, is_explicit, length, album, is_private) VALUES (1, 'Intro', 2, 2019, 'Selfmade', 'Progressive Metal', false, 165, 'Epic Shadows', false);
-- INSERT INTO metlify.song (id, title, user_id, released, label, genre, is_explicit, length, album, is_private) VALUES (2, 'Ready to Kill', 2, 2019, 'Selfmade', 'Progressive Metal', true, 354, 'Epic Shadows', false);
-- INSERT INTO metlify.song (id, title, user_id, released, label, genre, is_explicit, length, album, is_private) VALUES (3, 'TestSong1', 1, 2018, 'peter123', 'Hard Rock', false, 300, 'Test', true);
--
--
-- INSERT INTO metlify.playlist (id, title, user_id, genre, is_private, last_updated) VALUES (1, 'Meine Playlist', 1, 'Mixed', false, '20042020');
--
INSERT INTO metlify.user_song (user_id, song_id) VALUES (1, 1);
INSERT INTO metlify.user_song (user_id, song_id) VALUES (1, 2);
INSERT INTO metlify.user_song (user_id, song_id) VALUES (2, 1);
INSERT INTO metlify.user_song (user_id, song_id) VALUES (2, 2);

INSERT INTO metlify.user_playlist (user_id, playlist_id) VALUES (1, 1);
--
-- INSERT INTO metlify.playlist_song (playlist_id, song_id) VALUES (1, 1);
-- INSERT INTO metlify.playlist_song (playlist_id, song_id) VALUES (1, 2);
-- INSERT INTO metlify.playlist_song (playlist_id, song_id) VALUES (1, 3);