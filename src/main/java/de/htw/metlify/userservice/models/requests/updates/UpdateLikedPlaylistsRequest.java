package de.htw.metlify.userservice.models.requests.updates;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Set;

public class UpdateLikedPlaylistsRequest {

    @NotNull
    @Positive
    int id;
    @NotNull
    Set<Integer> newLikedPlaylistIds;

    public UpdateLikedPlaylistsRequest(int id, Set<Integer> newLikedPlaylists) {
        this.id = id;
        this.newLikedPlaylistIds = newLikedPlaylists;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Integer> getNewLikedPlaylistIds() {
        return newLikedPlaylistIds;
    }

    public void setNewLikedPlaylistIds(Set<Integer> newLikedPlaylistIds) {
        this.newLikedPlaylistIds = newLikedPlaylistIds;
    }
}
