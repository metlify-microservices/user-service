package de.htw.metlify.userservice.controllers;

import de.htw.metlify.userservice.exceptions.UserEmailAlreadyInUseException;
import de.htw.metlify.userservice.exceptions.UsernameAlreadyInUseExcection;
import de.htw.metlify.userservice.models.User;
import de.htw.metlify.userservice.models.UserDTO;
import de.htw.metlify.userservice.models.requests.updates.*;
import de.htw.metlify.userservice.services.MessagingService;
import de.htw.metlify.userservice.services.RegistrationService;
import de.htw.metlify.userservice.services.UserService;
import de.htw.metlify.userservice.utils.JwtTokenValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final RegistrationService registrationService;
    private final JwtTokenValidator tokenValidator;
    private final MessagingService messagingService;

    @Autowired
    public UserController(UserService userService, RegistrationService registrationService, JwtTokenValidator tokenValidator, MessagingService messagingService) {
        this.userService = userService;
        this.registrationService = registrationService;
        this.tokenValidator = tokenValidator;
        this.messagingService = messagingService;
    }

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        List<UserDTO> users = userService.findAllUsers();
        if (users.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(users);
        }
    }

    @GetMapping("/getBy")
    public ResponseEntity<List<UserDTO>> getUsersByParameter(@RequestParam(name = "parameter") String parameter, @RequestParam(name = "value") String value) {
        try {
            List<UserDTO> userList = userService.getUserByParameter(parameter, value);

            if (userList.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            } else {
                return ResponseEntity.status(HttpStatus.OK).body(userList);
            }
        } catch (InvalidDataAccessApiUsageException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable("id") Integer id) {
        UserDTO user = userService.findUserById(id);

        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(user);
        }
    }

    @PostMapping("/register")
    public ResponseEntity<User> registerUser(@Valid @RequestBody User user) {
        User registeredUser = registrationService.registerUser(user);

        return ResponseEntity.status(HttpStatus.CREATED).body(registeredUser);
    }

    @PutMapping("/username")
    public ResponseEntity<UserDTO> updateUsername(@Valid @RequestBody UpdateUsernameRequest request,
                                               @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorized(request.getId(), authHeader)) {
                UserDTO updatedUser = userService.updateUsername(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (UsernameAlreadyInUseExcection e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }


    @PutMapping("/email")
    public ResponseEntity<UserDTO> updateUserEmail(@Valid @RequestBody UpdateEmailRequest request,
                                                @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        try {
            if (userIsAuthorized(request.getId(), authHeader)) {
                UserDTO updatedUser = userService.updateEmail(request);

                return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        } catch (UserEmailAlreadyInUseException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PutMapping("/sex")
    public ResponseEntity<UserDTO> updateUserSex(@Valid @RequestBody UpdateSexRequest request,
                                              @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            UserDTO updatedUser = userService.updateSex(request);

            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PutMapping("/location")
    public ResponseEntity<UserDTO> updateUserLocation(@Valid @RequestBody UpdateLocationRequest request,
                                                   @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            UserDTO updatedUser = userService.updateLocation(request);

            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PutMapping("/favouriteGenre")
    public ResponseEntity<UserDTO> updateUserFavouriteGenre(@Valid @RequestBody UpdateFavouriteGenreRequest request,
                                                         @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            UserDTO updatedUser = userService.updateFavouriteGenre(request);

            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PutMapping("/favouriteBand")
    public ResponseEntity<UserDTO> updateUserFavouriteBand(@Valid @RequestBody UpdateFavouriteBandRequest request,
                                                        @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            UserDTO updatedUser = userService.updateFavouriteBand(request);

            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PutMapping("/isPrivate")
    public ResponseEntity<UserDTO> updateUserIsPrivate(@Valid @RequestBody UpdateIsPrivateRequest request,
                                                    @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            UserDTO updatedUser = userService.updateIsPrivate(request);

            return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }


    @PutMapping("/likedSongs")
    public ResponseEntity<UserDTO> updateUserLikedSongs(@Valid @RequestBody UpdateLikedSongsRequest request,
                                                     @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        int userId = request.getId();
        if (userIsAuthorized(userId, authHeader)) {
            if (songsAreInDatabase(request.getNewLikedSongsIds())) {
                Set<Integer> newLikedSongs = request.getNewLikedSongsIds();
                UserDTO updatedUser = userService.updateLikedSongs(userId, newLikedSongs);
                return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private boolean songsAreInDatabase(Set<Integer> songIds) {
        return messagingService.areSongsInDatabase(songIds);
    }

    @PutMapping("/likedPlaylists")
    public ResponseEntity<UserDTO> updateUserLikedPlaylists(@Valid @RequestBody UpdateLikedPlaylistsRequest request,
                                                         @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        int userId = request.getId();
        if (userIsAuthorized(userId, authHeader)) {
            if (playListsAreInDatabase(request.getNewLikedPlaylistIds())) {
                Set<Integer> newLikedPlaylists = request.getNewLikedPlaylistIds();
                UserDTO updatedUser = userService.updateLikedPlaylists(userId, newLikedPlaylists);
                return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private boolean playListsAreInDatabase(Set<Integer> playListIds) {
        return messagingService.arePlaylistsInDatabase(playListIds);
    }


    @PutMapping("/password")
    public ResponseEntity<User> updateUserPassword(@Valid @RequestBody NewPasswordRequest request,
                                                   @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(request.getId(), authHeader)) {
            registrationService.updateUserPassword(request);

            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<User> deleteUserById(@PathVariable("id") Integer id,
                                               @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader) {
        if (userIsAuthorized(id, authHeader)) {
            userService.deleteUser(id);
            messagingService.deleteUserInAllServices(id);

            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }


    private boolean userIsAuthorized(int id, String authHeader) {
        return id == tokenValidator.getIdFromAuthHeader(authHeader);
    }

    // This is needed for elegant error message when bean validation fails on registration
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleError(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return errors;
    }
}
