package de.htw.metlify.userservice.models.events;


import java.util.Set;

public class AreSongsInDatabaseEvent {

    private Set<Integer> songIds;

    public AreSongsInDatabaseEvent(Set<Integer> songIds) {
        this.songIds = songIds;
    }

    public AreSongsInDatabaseEvent() {

    }

    public Set<Integer> getSongIds() {
        return songIds;
    }

    public void setSongIds(Set<Integer> songIds) {
        this.songIds = songIds;
    }
}
