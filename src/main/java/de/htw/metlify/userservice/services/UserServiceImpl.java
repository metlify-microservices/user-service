package de.htw.metlify.userservice.services;

import de.htw.metlify.userservice.exceptions.UserEmailAlreadyInUseException;
import de.htw.metlify.userservice.exceptions.UsernameAlreadyInUseExcection;
import de.htw.metlify.userservice.models.User;
import de.htw.metlify.userservice.models.UserDTO;
import de.htw.metlify.userservice.models.requests.updates.*;
import de.htw.metlify.userservice.repositories.UserRepository;
import de.htw.metlify.userservice.specifications.UserSpecification;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private MessagingService messagingService;
    private final boolean IS_NOT_PRIVATE = false;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, MessagingService messagingService) {
        this.userRepository = userRepository;
        this.messagingService = messagingService;
    }

    @Override
    public UserDTO findUserById(Integer id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isEmpty() || userOptional.get().isPrivate()) {
            return null;
        } else {
            User user = userOptional.get();
            UserDTO userDTO = createUserDtoFromUser(user);

            return userDTO;
         }
    }

    private UserDTO createUserDtoFromUser(User user) {
        Integer userId = user.getId();

        Set<Integer> likedSongs = userRepository.findLikedSongsFromUser(userId);
        Set<Integer> likedPlaylists = userRepository.findLikedPlaylistsFromUser(userId);
        Set<Integer> songs = messagingService.getSongsFromUser(userId);
        Set<Integer> playlists = messagingService.getPlaylistsFromUser(userId);

        ModelMapper modelMapper = new ModelMapper();
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);

        userDTO.setLikedSongs(likedSongs);
        userDTO.setLikedPlaylists(likedPlaylists);
        userDTO.setSongs(songs);
        userDTO.setPlaylists(playlists);

        return userDTO;
    }

    @Override
    public List<UserDTO> findAllUsers() {
        List<User> users = userRepository.findAllByIsPrivate(IS_NOT_PRIVATE);
        List<UserDTO> userDTOS = mapFromUsersToUserDTOs(users);
        return userDTOS;
    }

    private List<UserDTO> mapFromUsersToUserDTOs(List<User> users) {
        List<UserDTO> userDTOS = new ArrayList<>();
        for (User user : users) {
            UserDTO userDTO = createUserDtoFromUser(user);

            userDTOS.add(userDTO);
        }
        return userDTOS;
    }

    @Override
    public List<UserDTO> getUserByParameter(String parameter, String value) {
        UserSpecification us = new UserSpecification(parameter, value);
        List<User> users = userRepository.findAll(us);
        List<User> publicUsers = filterOutPrivateUsers(users);
        List<UserDTO> userDTOS = mapFromUsersToUserDTOs(publicUsers);

        return userDTOS;
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserDTO updateUsername(UpdateUsernameRequest request) throws UsernameAlreadyInUseExcection {
        String newUsername = request.getNewUsername();
        User userFromDB = userRepository.findByIdAndIsPrivate(request.getId(), false);

        if (!isUsernameAlreadyInUse(newUsername)) {
            userFromDB.setUsername(newUsername);

            return createUserDtoFromUser(userRepository.save(userFromDB));
        } else {
            throw new UsernameAlreadyInUseExcection();
        }
    }

    @Override
    public UserDTO updateEmail(UpdateEmailRequest request) throws UserEmailAlreadyInUseException {
        String newEmail = request.getNewEmail();
        User userFromDB = userRepository.findById(request.getId());

        if (!isEmailAlreadyInUse(newEmail)) {
            userFromDB.setEmail(newEmail);

            return createUserDtoFromUser(userRepository.save(userFromDB));
        } else {
            throw new UserEmailAlreadyInUseException();
        }
    }

    @Override
    public UserDTO updateSex(UpdateSexRequest request) {
        String newSex = request.getNewSex();
        User userFromDB = userRepository.findById(request.getId());
        userFromDB.setSex(newSex);

        return createUserDtoFromUser(userRepository.save(userFromDB));
    }

    @Override
    public UserDTO updateLocation(UpdateLocationRequest request) {
        String newLocation = request.getNewLocation();
        User userFromDB = userRepository.findById(request.getId());
        userFromDB.setLocation(newLocation);

        return createUserDtoFromUser(userRepository.save(userFromDB));
    }

    @Override
    public UserDTO updateFavouriteGenre(UpdateFavouriteGenreRequest request) {
        String newFavouriteGenre = request.getNewFavouriteGenre();
        User userFromDB = userRepository.findById(request.getId());
        userFromDB.setFavouriteGenre(newFavouriteGenre);

        return createUserDtoFromUser(userRepository.save(userFromDB));
    }

    @Override
    public UserDTO updateFavouriteBand(UpdateFavouriteBandRequest request) {
        String newFavouriteBand = request.getNewFavouriteBand();
        User userFromDB = userRepository.findById(request.getId());
        userFromDB.setFavouriteBand(newFavouriteBand);

        return createUserDtoFromUser(userRepository.save(userFromDB));
    }

    @Override
    public UserDTO updateIsPrivate(UpdateIsPrivateRequest request) {
        boolean isPrivate = request.isPrivate();
        User userFromDB = userRepository.findById(request.getId());
        userFromDB.setPrivate(isPrivate);

        return createUserDtoFromUser(userRepository.save(userFromDB));
    }

    @Override
    public UserDTO updateLikedSongs(int userId, Set<Integer> newLikedSongs) {
        userRepository.deleteLikedSongs(userId);
        for (Integer newLikedSongId: newLikedSongs) {
            userRepository.addLikedSongs(userId, newLikedSongId);
        }
        return findUserById(userId);
    }

    @Override
    public UserDTO updateLikedPlaylists(int userId, Set<Integer> newLikedPlaylists) {
        userRepository.deleteLikedPlaylists(userId);
        for (Integer newLikedPlaylistId: newLikedPlaylists) {
            userRepository.addLikedPlaylist(userId, newLikedPlaylistId);
        }
        return findUserById(userId);
    }


    @Override
    public boolean isEmailAlreadyInUse(String email) {
        return userRepository.findByEmail(email) != null;
    }

    @Override
    public boolean isUsernameAlreadyInUse(String username) {
        return userRepository.findByUsername(username) != null;
    }


    private List<User> filterOutPrivateUsers(List<User> users) {
        return users.stream().filter(user -> !user.isPrivate()).collect(Collectors.toList());
    }

    @Override
    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
        userRepository.deleteLikedSongs(id);
        userRepository.deleteLikedPlaylists(id);

    }

    @Override
    public void deleteUserPlaylist(int playlistId) {
        userRepository.deleteLikedPlaylistsByPlaylistId(playlistId);
    }

    @Override
    public void deleteUserSong(int songId) {
        userRepository.deleteLikedSongsBySongId(songId);
    }
}
