package de.htw.metlify.userservice.models.requests.updates;

import javax.validation.constraints.*;

public class UpdateUsernameRequest {

    @NotNull
    @Positive
    private int id;
    @NotNull
    @NotEmpty
    @NotBlank
    @Size(min = 2, max = 40, message = "new username must be at least 2 characters long")
    private String newUsername;

    public UpdateUsernameRequest(int id, String newUsername) {
        this.id = id;
        this.newUsername = newUsername;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }
}
